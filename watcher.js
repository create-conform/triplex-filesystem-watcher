/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex Filesystem Watcher
//
//    Triplex module that submits jobs to a dispatcher when files are added/updated/removed
//    from the filesystem.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
    Error =        require("error");
var type =         require("type");
var chokidar =     require("chokidar");


/////////////////////////////////////////////////////////////////////////////////////////////
//
// TriplexFilesystemWatcher Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function TriplexFilesystemWatcher(options, shared) {
    if (!(this instanceof TriplexFilesystemWatcher)) return new TriplexFilesystemWatcher(options, shared);

    var self = this;
    var watcher;

    shared =            shared || {};
    shared.dispatcher = shared.dispatcher || {};

    this.options =      options || {};


    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            // self.options.dispatcher can be a named instance running on the
            // current triplex instance, or a url to a remote dispatcher.
            // This feature eneds to be implemented in dispatcher.

            //options.dispatcher -> named instance/url
            //options.path -> path to watch
            //options.job -> named template/job object

            // create chokidar options
            var cOpts = {};
            cOpts.awaitWriteFinish =       self.options.awaitWriteFinish;
            cOpts.ignorePermissionErrors = self.options.ignorePermissionErrors;
            cOpts.depth =                  self.options.depth;
            cOpts.disableGlobbing =        self.options.disableGlobbing;
            cOpts.ignoreInitial =          self.options.ignoreInitial;

            // Initialize watcher.
            var watcher = chokidar.watch(self.options.path, cOpts);
            watcher.on("add", function fileAdded(path) {
                var params = {};
                params[self.options.jobParameter || "path"] = path;

                if (type.isString(self.options.dispatcher) || !self.options.dispatcher) {
                    shared.dispatcher[self.options.dispatcher? self.options.dispatcher : "default"].queueJob(self.options.job, params);
                }
                else {
                    throw "Not Implemented";
                }
            })

            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            // stop the chokidar watcher, and destroy
            if (watcher) {
                watcher.close();
                watcher = null;
            }

            resolve();
        });
    };
};


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = TriplexFilesystemWatcher;